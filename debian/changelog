libjs-twitter-bootstrap-datepicker (1.3.1+dfsg1-5) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Move source package lintian overrides to debian/source.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Make "Files: *" paragraph the first in the copyright file.

  [ Thomas Goirand ]
  * Switch upstream URLs to https://github.com/uxsolutions/bootstrap-datepicker
    in d/rules, d/copyright and d/control.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Thomas Goirand <zigo@debian.org>  Fri, 10 Apr 2020 00:00:15 +0200

libjs-twitter-bootstrap-datepicker (1.3.1+dfsg1-4) unstable; urgency=medium

  * Removed libjs-twitter-bootstrap runtime depends, now the package only
    depends on bootstrap 3. (Closes: #843912).

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Oct 2018 11:09:02 +0100

libjs-twitter-bootstrap-datepicker (1.3.1+dfsg1-3) unstable; urgency=medium

  * Remove dependency on old version of bootstrap 2. (Closes: #911660).
  * Remove installation of files for bootstrap 2.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Oct 2018 12:07:09 +0200

libjs-twitter-bootstrap-datepicker (1.3.1+dfsg1-2) unstable; urgency=medium

  * Deprecate Priority: extra as per policy.
  * Ran wrap-and-sort -bast.
  * Removed Pre-Depends: dpkg (>= 1.15.6~).
  * Point VCS URLs to Salsa.
  * Add libjs-bootstrap (ie: bootstrap 3) as possible dependency
    (Closes: #843912). Also install files in the bootstrap 3 folder.
  * Use https for upstream VCS URLs in d/rules.
  * Removed "Commands not to run" from d/rules.
  * Removed debian/gbp.conf (useless).
  * Add debian/source.lintian-overrides to accept minified JS for the doc, that
    we don't package anyways.

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 Sep 2018 10:45:57 +0200

libjs-twitter-bootstrap-datepicker (1.3.1+dfsg1-1) unstable; urgency=medium

  * New upstream release:
    - Using the fork from https://github.com/eternicode/bootstrap-datepicker
  * Standards-Version is now 3.9.6 (no change).
  * Added a watch file.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Oct 2014 15:13:46 +0800

libjs-twitter-bootstrap-datepicker (0.0.0.1-1) unstable; urgency=medium

  * Initial release (Closes: #754394).

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 Jul 2014 23:26:54 +0800
